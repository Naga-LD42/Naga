// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Naga_Segment.h"
#include "CH_Base.generated.h"

UCLASS(Blueprintable)
class NAGA_API ACH_Base : public ACharacter
{
	GENERATED_BODY()

public:
    UPROPERTY( BlueprintReadOnly, VisibleAnywhere, Category = "Naga", meta = (DisplayName = "NagaLength") )
    uint8 nSegments;

    uint8 desiredSegments;

    UPROPERTY( BlueprintReadWrite, VisibleAnywhere, Category = "Naga" )
    UClass* segmentClass;

private:
    ANaga_Segment* pBodySegment;

public:
	// Sets default values for this character's properties
    ACH_Base();

    UFUNCTION( BlueprintCallable, Category = "Naga" )
    void MoveHead( FVector deltaLocation );

    void AddBodySegment(FVector newPos, FVector pos );

    UFUNCTION( BlueprintCallable, Category = "Naga" )
    void AteOne();

    UFUNCTION( BlueprintCallable, Category = "Naga" )
    void GotAte();

    UFUNCTION( BlueprintCallable, Category = "Naga" )
    void RemoveBodySegments( ANaga_Segment* segment );

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	
	
};
