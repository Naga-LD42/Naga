// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Naga_Segment.generated.h"

UCLASS(Blueprintable)
class NAGA_API ANaga_Segment : public AActor
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintCallable, Category = "Naga Segment")
    bool IsTail();
	
    UPROPERTY( BlueprintReadOnly, Category = "Naga Segment" )
    ANaga_Segment* pNextSegment;

public:
    void MoveSegment( FVector headPos, FVector newPos );
    void SetNextSegment( ANaga_Segment* _pNextSegment );

	UFUNCTION(BlueprintImplementableEvent, Category = "Naga Segment")
	void UpdateOrientation(FVector head_location, FVector body_location, FVector tail_location);

public:	
	// Sets default values for this actor's properties
	ANaga_Segment();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	
	
};
