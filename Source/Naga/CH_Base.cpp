// Fill out your copyright notice in the Description page of Project Settings.

#include "CH_Base.h"


// Sets default values
ACH_Base::ACH_Base()
    : pBodySegment( nullptr )
    , nSegments( 1 )
{
	desiredSegments = 3;
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

void ACH_Base::MoveHead( FVector deltaLocation ) {

    FVector oldPos = GetActorLocation();
    AddActorWorldOffset( deltaLocation );
    FVector newPos = GetActorLocation();

	if (nSegments < desiredSegments) {
		AddBodySegment(newPos, oldPos);
	}
	else if (nSegments > desiredSegments && pBodySegment != nullptr) {
		ANaga_Segment* pNewTail = pBodySegment;
		while (pNewTail->pNextSegment != nullptr) {
			pNewTail = pNewTail->pNextSegment;
		}

		RemoveBodySegments(pNewTail);
        pBodySegment->MoveSegment( newPos, oldPos );
	}
    else if( pBodySegment != nullptr ){
        pBodySegment->MoveSegment( newPos, oldPos );
    }
}

void ACH_Base::AteOne() {
	desiredSegments = desiredSegments + 3;
}

void ACH_Base::GotAte() {
	desiredSegments = desiredSegments - 2;
	if (desiredSegments < 3) {
		desiredSegments = 3;
	}
}


void ACH_Base::AddBodySegment(FVector newPos, FVector pos ) {
    FRotator rot( 0 );
    ANaga_Segment* pNewSegment = GetWorld()->SpawnActor<ANaga_Segment>( segmentClass, pos, rot );

    ++nSegments;

    if( pBodySegment == nullptr ) {
        pBodySegment = pNewSegment;
		pNewSegment->UpdateOrientation(newPos, pos, pBodySegment->GetActorLocation());
    } else {
        pNewSegment->SetNextSegment( pBodySegment );
		pNewSegment->UpdateOrientation(newPos, pos, pBodySegment->GetActorLocation());
        pBodySegment = pNewSegment;
    }

}

void ACH_Base::RemoveBodySegments( ANaga_Segment* segment ) {

    bool bShouldDestroy = false;
    ANaga_Segment* pNewTail = pBodySegment;
    
    while( pNewTail != nullptr ) {
        if( pNewTail->pNextSegment == segment ) {
            break;
        }
        pNewTail = pNewTail->pNextSegment;
    }

    ANaga_Segment* pDeadSegment = pNewTail->pNextSegment;
    pNewTail->SetNextSegment( nullptr );

    while( pDeadSegment != nullptr ) {
        pDeadSegment->Destroy();
        pDeadSegment = pDeadSegment->pNextSegment;
        --nSegments;

		if (nSegments < desiredSegments) {
			desiredSegments = nSegments;
		}
    }

    nSegments = nSegments > 3 ? nSegments : 3;
}

// Called when the game starts or when spawned
void ACH_Base::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ACH_Base::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ACH_Base::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

