// Fill out your copyright notice in the Description page of Project Settings.

#include "Naga_Segment.h"

void ANaga_Segment::MoveSegment( FVector head_loc, FVector new_location ) {
	FVector oldPos = GetActorLocation();
    SetActorLocation(new_location);
	
    if( pNextSegment != nullptr ) {
        pNextSegment->MoveSegment(new_location, oldPos);
    }

	UpdateOrientation(head_loc, new_location, oldPos);
}

void ANaga_Segment::SetNextSegment( ANaga_Segment* _pNextSegment ) {

    pNextSegment = _pNextSegment;
}

bool ANaga_Segment::IsTail() {
	return pNextSegment == nullptr;
}

// Sets default values
ANaga_Segment::ANaga_Segment()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ANaga_Segment::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ANaga_Segment::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

